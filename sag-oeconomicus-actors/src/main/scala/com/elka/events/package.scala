package com.elka

package object events {
  class NewDay
  class EndOfSimulation
}
