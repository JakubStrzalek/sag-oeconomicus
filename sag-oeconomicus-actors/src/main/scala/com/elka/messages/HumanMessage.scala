package com.elka.messages

import com.elka.domain.{BuyOffert, SalesObject, Supply}

abstract class HumanMessage()
case class BoughtHumanMessage(boughtObject: (Supply, Int)) extends HumanMessage
case class SoldHumanMessage(soldObject: (SalesObject,Double)) extends HumanMessage