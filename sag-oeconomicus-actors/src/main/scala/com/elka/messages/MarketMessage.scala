package com.elka.messages

import akka.actor.ActorRef
import com.elka.domain.{BuyOffert, SalesObject}

abstract class MarketMessage()

case class BuyMarketMessage(buyOffert: BuyOffert) extends MarketMessage()
case class SellMarketMessage(salesObject: SalesObject) extends MarketMessage()
