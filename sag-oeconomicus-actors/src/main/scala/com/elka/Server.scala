package com.elka

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, ActorSystem, Cancellable, Props}
import akka.agent.Agent
import com.elka.actors.{HumanActor, MarketActor}
import com.elka.domain.{AbstractActor, GlobalConfig, Human, Market}
import com.elka.domain.parser.{ConfigParser, ConfigParserData}
import com.elka.events.{EndOfSimulation, NewDay}
import com.elka.messages.BuyMarketMessage

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import scala.collection.JavaConverters._
import scala.collection.mutable

object ActorTypes extends Enumeration {
  val MarketType,HumanType = Value
}

object Server {

  val system : ActorSystem = ActorSystem.create("system")

  var marketId : String = "market"

  var config : GlobalConfig = null
  var currentDay = 1

  var cancellable : Cancellable = null

  def main(args : Array[String]) : Unit = {
    val parser : ConfigParser = new ConfigParser

    val configParserData : ConfigParserData = parser.parseXML(args(0))
    val domainActors = configParserData.getActors.asScala
    config = configParserData.getGlobalConfig

    val actors : List[(ActorRef, ActorTypes.Value)] = createActorsFromDomains(domainActors)

    subscribeHumanActors(actors.filter(_._2 == ActorTypes.HumanType).map(_._1))

    cancellable = system.scheduler
      .schedule(Duration.create(config.getNewDayInterval, TimeUnit.SECONDS),
        Duration.create(config.getNewDayInterval, TimeUnit.SECONDS))(sendNextDayEvent())

  }

  def createActorsFromDomains(domainActors: mutable.Buffer[AbstractActor]) : List[(ActorRef,ActorTypes.Value)] = {
    domainActors.map {
      case x: Human =>
        (system.actorOf(Props.create(classOf[HumanActor],x,marketId),"human" + x.getId.toString),ActorTypes.HumanType)
      case x: Market =>
        (system.actorOf(Props.create(classOf[MarketActor],x),marketId),ActorTypes.MarketType)
    }.to[List]
  }

  def subscribeHumanActors(humanActors : List[ActorRef]) : Unit = {
    humanActors.foreach({
      x =>
      system.eventStream.subscribe(x, classOf[NewDay])
      system.eventStream.subscribe(x, classOf[EndOfSimulation])
    })
  }

  def sendNextDayEvent() : Unit = {
    system.eventStream.publish(new NewDay)
    currentDay += 1

    if(currentDay == config.getNumberOfDays) {
      system.eventStream.publish(new EndOfSimulation)
      cancellable.cancel()
      system.scheduler.scheduleOnce(Duration.create(config.getNewDayInterval, TimeUnit.SECONDS))(system.terminate())
    }
  }
}
