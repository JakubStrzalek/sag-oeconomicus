package com.elka.actors

import akka.actor.UntypedActor
import akka.event.Logging
import com.elka.domain.{BuyOffert, Market, SalesObject}
import com.elka.messages._

import scala.collection.JavaConversions._

class MarketActor(val domain: Market) extends UntypedActor {
  val log = Logging(context.system, this)

  @scala.throws[Exception](classOf[Exception])
  override def onReceive(message: Any): Unit = message match {
    case x: BuyMarketMessage =>
      domain.addNewBuyOffert(x.buyOffert)
      matchOffersAndSend()
    case x: SellMarketMessage =>
      domain.addNewSalesObject(x.salesObject)
      matchOffersAndSend()
    case _ => println("Unknown object type")
  }

  def matchOffersAndSend(): Unit =
    domain.matchAll().map(x => (x.getKey, x.getValue)).foreach(acceptAndSendBackMoney)

  def acceptAndSendBackMoney(deal: (BuyOffert, SalesObject)): Unit = {
    getContext().system.actorSelection(humanId(deal._1.getHumanId)) ! BoughtHumanMessage((deal._2.getSupply,deal._2.getAmountOfSupplies))
    getContext().system.actorSelection(humanId(deal._2.getHumanId)) ! SoldHumanMessage(deal._2,deal._1.getMoney)
  }

  private def humanId(id: Int): String = "user/human" + id
}
