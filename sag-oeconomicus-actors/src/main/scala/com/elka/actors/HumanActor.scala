package com.elka.actors

import akka.actor.UntypedActor
import com.elka.domain.{BuyOffert, Communication, Human, SalesObject}
import com.elka.events.{EndOfSimulation, NewDay}
import com.elka.messages.{BoughtHumanMessage, BuyMarketMessage, SellMarketMessage, SoldHumanMessage}

class HumanActor(val domain: Human, marketId: String) extends UntypedActor with Communication {

  @Override
  def sendToMarket(buyOffert: BuyOffert) =
    getContext().system.actorSelection("user/" + marketId) ! new BuyMarketMessage(buyOffert)

  @Override
  def sendToMarket(salesObject: SalesObject) =
    getContext().system.actorSelection("user/" + marketId) ! new SellMarketMessage(salesObject)

  override def preStart() : Unit = {
    domain.setCommunication(this)
    domain.introduceMe()
    domain.makeDecision()
  }

  var endOfSimulation = false

  @scala.throws[Exception](classOf[Exception])
  override def onReceive(message: Any): Unit = message match {
    case x: BoughtHumanMessage =>
      if(!endOfSimulation) domain.addSupplies(x.boughtObject._1,x.boughtObject._2)
    case x: SoldHumanMessage =>
      if(!endOfSimulation) domain.increaseMoney(x.soldObject._1,x.soldObject._2)
    case x: NewDay => domain.makeDecision()
    case x: EndOfSimulation =>
      endOfSimulation = true
      domain.writeEndingLogs()
    case _ => println("Unknown object type")
  }


}
