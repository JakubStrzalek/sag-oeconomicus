package com.elka.domain;

import java.util.*;

public class Human extends AbstractActor implements StrategyPlan {
    private final int MONEY_LIMIT = 5;
    private final int SUPPLY_NOT_NEEDED = 10;
    private final String FIRST_DEMAND = "First";
    private final int SLEEP_HOURS = 6;
    private final String SECOND_DEMAND = "Second";
    private int id;
    private String name;
    private Map<DemandName, Demand> demands;
    private Job job;
    private Map<DemandName, List<Supply>> supplies;
    private double money;
    private double actionPoints;
    private List<SalesObject> salesObjects;
    private List<BuyOffert> buyOfferts;
    private boolean hasWorked = false;
    private boolean hasSlept = false;
    private int dayNumber = 0;

    private Communication communication;

    public Human(int id, Map<DemandName, Demand> demands, Job job, Map<DemandName, List<Supply>> supplies, double money) {
        this.id = id;
        this.demands = demands;
        this.job = job;
        this.supplies = supplies;
        this.money = money;
        actionPoints = 24;
        salesObjects = new ArrayList<>();
        buyOfferts = new ArrayList<>();
        Logger.logIdentity(this);
    }

    public Human() {
        salesObjects = new ArrayList<>();
        buyOfferts = new ArrayList<>();
        supplies = new HashMap<>();
        for (DemandName demandName : DemandName.values()) {
            supplies.put(demandName, new ArrayList<Supply>());
        }
    }

    public void introduceMe() {
        Logger.logIdentity(this);
    }

    public void writeEndingLogs() {
        Logger.logHumanDemands(this);
    }

    public void setCommunication(Communication communication) {
        this.communication = communication;
    }

    public void consumeSupply(DemandName demandName, String supplyName) {
        List<Supply> supplyList = supplies.get(demandName);
        for (int i = 0; i < supplyList.size(); i++) {
            if (supplyList.get(i).getName().equals(supplyName)) {
                demands.get(supplyList.get(i).getBuff().getDemandName())
                        .fulfill(supplyList.get(i).getBuff().getValue());
                Logger.logConsumption(this, supplyList.get(i));
                actionPoints -= 1;
                if (supplyList.get(i).isOneTime()) {
                    supplies.get(demandName).remove(i);
                }
                break;
            }
        }
    }

    public void work(int hours) {
        if (job.doProduceSupply()) {
            List<Supply> newSupplies = job.workForSupply(getId(), hours);
            for (Supply item : newSupplies) {
                addSupply(item.getBuff().getDemandName(), item);
            }
            actionPoints -= newSupplies.size();
            if(demands.get(DemandName.KARIERA) != null) {
                demands.get(DemandName.KARIERA).fulfill(newSupplies.size());
            }
        } else {
            int earnedMoney = job.workForMoney(getId(), hours);
            if (earnedMoney > 0) {
                money += earnedMoney;
                int time = hours > job.getMaxHoursPerDay() ? job.getMaxHoursPerDay() : hours;
                actionPoints -= time;
                if(demands.get(DemandName.KARIERA) != null) {
                    demands.get(DemandName.KARIERA).fulfill(time);
                }
            }
        }
        hasWorked = true;
    }

    public void sleep(int hours) {
        if (0 < hours) {
            int sleepHours = hours;
            if (hours > actionPoints) {
                sleepHours = (int) actionPoints;
                Logger.logSleep(getId(), sleepHours);
            }
            demands.get(DemandName.SEN).fulfill(sleepHours);
            actionPoints -= sleepHours;
            hasSlept = true;
        }
    }

    /**
     * Function that returns two most important Demands.
     *
     * @return Map with keys "First" (the most important Demand) and "Second" (second demand).
     */
    public Map<String, Demand> getTwoMostImportandDemands() {
        Demand toReturnHigh = null,
                toReturnLow = null;

        for (DemandName item : DemandName.values()) {
            if (toReturnHigh == null || (!demands.isEmpty() && demands.get(item) != null && demands.get(item).getValue() >= getDemandValue(toReturnHigh))) {
                toReturnLow = toReturnHigh;
                toReturnHigh = demands.get(item);
            } else if (toReturnLow == null || (!demands.isEmpty() && demands.get(item) != null && demands.get(item).getValue() > getDemandValue(toReturnLow))) {
                toReturnLow = demands.get(item);
            }
        }

        Map<String, Demand> toReturnFinal = new HashMap<>();
        toReturnFinal.put(FIRST_DEMAND, toReturnHigh);
        toReturnFinal.put(SECOND_DEMAND, toReturnLow);
        return toReturnFinal;
    }

    private int getDemandValue(Demand d) {
        if (d == null) {
            return 0;
        }

        return d.getValue();
    }

    public void addSupply(DemandName name, Supply value) {
        if (getSupplies().get(name) != null) {
            getSupplies().get(name).add(value);
        } else {
            ArrayList<Supply> list = new ArrayList<>();
            list.add(value);

            getSupplies().put(name, list);
        }
    }

    public int getId() {
        return id;
    }

    public Map<DemandName, Demand> getDemands() {
        return demands;
    }


    public Job getJob() {
        return job;
    }

    public Map<DemandName, List<Supply>> getSupplies() {
        makeSureSuppliesExists();
        return supplies;
    }

    private void makeSureSuppliesExists() {
        if (supplies == null) {
            supplies = new HashMap<>();
        }
    }

    public void addNewSalesObject(SalesObject newSalesObject) {
        salesObjects.add(newSalesObject);
    }

    public void addNewBuyOffert(BuyOffert buyOffert) {
        buyOfferts.add(buyOffert);
    }

    @Override
    public void makeDecision() {
        dayNumber += 1;
        Logger.logNewDay(getId(), dayNumber);
        Logger.logHumanDemands(this);
        resetActionPoints();
        checkTodaysNeeds();
        makeJobMarketDecision();
    }

    private void resetActionPoints() {
        actionPoints = 24;
        hasWorked = false;
        hasSlept = false;
    }

    private void checkTodaysNeeds() {
        Map<String, Demand> todayNeeds = getTwoMostImportandDemands();
        Set<String> needsNames = todayNeeds.keySet();
        Set<DemandName> suppliesNames = getSupplies().keySet();
        for (String s : needsNames) {
            if(todayNeeds.get(s).getDemandName().equals(DemandName.SEN)){
                sleep(todayNeeds.get(s).getValue());
            }
            else {
                for (DemandName d : suppliesNames) {
                    if (todayNeeds.get(s) != null && d.getName().equals(todayNeeds.get(s).getDemandName().getName())) {
                        fulfillDemand(s, d, todayNeeds);
                    }
                }
            }

        }
    }

    private void makeJobMarketDecision() {
        createSalesObjects();
        if (money > MONEY_LIMIT) {
            createBuyOffers();
        }
        work(calculateWorkingHours());
    }

    private void createSalesObjects() {
        List<Demand> lowestDemands = getLowestOrientedDemandList();
        for (Demand d : lowestDemands) {
            if (d.getValue() < SUPPLY_NOT_NEEDED) {
                List<SalesObject> objects = makeSalesObject(d);
//                salesObjects.addAll(objects);
                for (SalesObject so : objects) {
                    if(so.getSupply() != null)
                        communication.sendToMarket(so);
                }
            }
        }
    }

    private List<SalesObject> makeSalesObject(Demand d) {
        List<SalesObject> toReturn = new ArrayList<>();
        Map<String, Integer> pickedSupplies = new HashMap<>();
        List<Supply> toRemoveSupplies = new ArrayList<>();
        if (!supplies.isEmpty() && supplies.get(d.getDemandName()) != null) {
            for (Supply item : supplies.get(d.getDemandName())) {
                if (pickedSupplies.get(item.getName()) != null) {
                    pickedSupplies.put(item.getName(), pickedSupplies.get(item.getName()) + 1);
                } else {
                    pickedSupplies.put(item.getName(), 1);
                }
                toRemoveSupplies.add(item);
            }
        }

        Set<String> supplyNames = pickedSupplies.keySet();
        for (String item : supplyNames) {
            toReturn.add(new SalesObject(this.id, getSupplyByName(item, d.getDemandName()), pickedSupplies.get(item)));
        }

        if (!supplies.isEmpty() && !toRemoveSupplies.isEmpty()) {
            clearSupplies(toRemoveSupplies, d.getDemandName());
        }

        Logger.logMarketSalesOffers(getId(), toReturn);
        return toReturn;
    }

    private Supply getSupplyByName(String name, DemandName demandName) {
        for (Supply item : supplies.get(demandName)) {
            if (item.getName() == name) {
                return item;
            }
        }

        return null;
    }

    private void createBuyOffers() {
        Map<String, Demand> twoDemands = getTwoMostImportandDemands();
        Demand firstDemand = twoDemands.get(FIRST_DEMAND);
        Demand secondDemand = twoDemands.get(SECOND_DEMAND);

        double firstMoneySacale = 0.0;
        double secondMoneyScale = 0.0;
        if (firstDemand != null)
            firstMoneySacale =(double) firstDemand.getValue() / (firstDemand.getValue() + secondDemand.getValue());

        if (secondDemand != null)
            secondMoneyScale =(double) secondDemand.getValue() / (firstDemand.getValue() + secondDemand.getValue());

        if (firstDemand != null && firstMoneySacale * money > MONEY_LIMIT) {
            double moneyForOne = firstDemand.getValue() / firstMoneySacale * money;
            BuyOffert firstOffer = new BuyOffert(id, firstMoneySacale * money, firstDemand.getDemandName(), moneyForOne);
            money -= firstMoneySacale * money;
            communication.sendToMarket(firstOffer);
            Logger.logMarketBuysOffers(getId(), firstOffer);
//            buyOfferts.add(firstOffer);
        } else if (firstDemand != null) {
            double moneyForOne = firstDemand.getValue() / firstMoneySacale * money;
            BuyOffert firstOffer = new BuyOffert(id, money, firstDemand.getDemandName(), moneyForOne);
            money -= money;
            communication.sendToMarket(firstOffer);
            Logger.logMarketBuysOffers(getId(), firstOffer);
//            buyOfferts.add(firstOffer);
        }
        if (secondDemand != null && secondMoneyScale * money > MONEY_LIMIT) {
            double moneyForOne = secondDemand.getValue() / secondMoneyScale * money;
            BuyOffert secondOffer = new BuyOffert(id, secondMoneyScale * money, secondDemand.getDemandName(), moneyForOne);
            money -= secondMoneyScale * money;
            communication.sendToMarket(secondOffer);
            int temp; // żeby nie pokazywalo duplikacji
            Logger.logMarketBuysOffers(getId(), secondOffer);
//            buyOfferts.add(secondOffer);
        }
    }

    private int calculateWorkingHours() {
        if (actionPoints >= job.getMaxHoursPerDay()) {
            return job.getMaxHoursPerDay();
        } else {
            return (int) actionPoints;
        }
    }

    public void clearSupplies(List<Supply> toDelete, DemandName key) {
        for(Supply supply: toDelete){
            supplies.get(key).remove(supply);
        }
    }

    public void fulfillDemand(String s, DemandName d, Map<String, Demand> todayNeeds) {
        List<Supply> suppliesToDelete = new ArrayList<>();
        for (Supply supply : supplies.get(d)) {
            if (todayNeeds.get(s).getValue() > 0) {
                if (supply.isOneTime()) {
                    suppliesToDelete.add(supply);
                }
                todayNeeds.get(s).fulfill(supply.getBuff().getValue());
                actionPoints -= 1;
                Logger.logConsumption(this, supply);
            } else {
                break;
            }
        }
        clearSupplies(suppliesToDelete, d);
    }

    public List<Demand> getLowestOrientedDemandList() {
        List<Demand> demandList = new ArrayList<>();
        Set<DemandName> keys = demands.keySet();
        for (DemandName d : keys) {
            demandList.add(demands.get(d));
        }
        Collections.sort(demandList);
        return demandList;
    }

    public void addSupplies(Supply supply, int amonut) {
        for (int i = 0; i < amonut; i++) {
            addSupply(supply.getBuff().getDemandName(), supply);
        }
        Logger.logIncomeSupplies(getId(), supply, amonut);
    }

    public void increaseMoney(SalesObject salesobject, Double money) {
        this.money += money;
        Logger.logIncomeMoney(getId(), money, salesobject);
    }

    public double getMoney() {
        return money;
    }

    public double getActionPoints() {
        return actionPoints;
    }

    @Override
    public String toString() {
        return "Human{" +
                "id=" + id +
                ", demands=" + demands +
                ", job=" + job +
                ", supplies=" + supplies +
                ", money=" + money +
                '}';
    }

    public void setDemands(Map<DemandName, Demand> demands) {
        this.demands = demands;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public void setSupplies(Map<DemandName, List<Supply>> supplies) {
        this.supplies = supplies;
    }

    public void setActionPoints(double actionPoints) {
        this.actionPoints = actionPoints;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public List<SalesObject> getSalesObjects() {
        return salesObjects;
    }

    public void setSalesObjects(List<SalesObject> salesObjects) {
        this.salesObjects = salesObjects;
    }

    public List<BuyOffert> getBuyOfferts() {
        return buyOfferts;
    }

    public void setBuyOfferts(List<BuyOffert> buyOfferts) {
        this.buyOfferts = buyOfferts;
    }
}
