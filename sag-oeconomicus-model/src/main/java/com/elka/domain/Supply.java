package com.elka.domain;

public class Supply {
    private final String name;
    private final double price;
    private final Buff buff;
    private final boolean isOneTime;

    public Supply(String name, double price, Buff buff, boolean isOneTime) {
        this.name = name;
        this.price = price;
        this.buff = buff;
        this.isOneTime = isOneTime;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Buff getBuff() {
        return buff;
    }

    public boolean isOneTime() {
        return isOneTime;
    }

    @Override
    public String toString() {
        return "Supply{" +
                "name=" + name +
                ", price=" + price +
                ", buff=" + buff +
                ", isOneTime=" + isOneTime +
                '}';
    }
}
