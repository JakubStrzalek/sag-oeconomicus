package com.elka.domain;

public enum DemandName {
    SEN("SEN"),
    JEDZENIE("JEDZENIE"),
    ROZRYWKA("ROZRYWKA"),
    KARIERA("KARIERA");

    private String name;

    DemandName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public DemandName matchName(String name){
        for(DemandName d: DemandName.values()){
            if(d.getName().equals(name)){
                return  d;
            }
        }

        return null;
    }
}
