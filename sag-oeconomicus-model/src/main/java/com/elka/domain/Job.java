package com.elka.domain;

import java.util.ArrayList;
import java.util.List;

public class Job {
    private final String name;
    private final int valuePerHour;
    private final int maxHoursPerDay;
    private final boolean produceSupply;
    private final Supply producedSupply;

    public Job(String name, int value, int maxPerDay) {
        this.name = name;
        this.valuePerHour = value;
        this.maxHoursPerDay = maxPerDay;
        this.produceSupply = false;
        this.producedSupply = null;
    }

    public Job(String name, int value, int maxPerDay, Supply supply) {
        this.name = name;
        this.valuePerHour = value;
        this.maxHoursPerDay = maxPerDay;
        this.produceSupply = true;
        this.producedSupply = supply;
    }

    public int workForMoney(int humanId, int hours) {
        if(produceSupply) return 0;
        int workedHours = calculateHours(hours);
        if(workedHours > 0) {
            Logger.logWork(humanId, this, workedHours);
        }
        return (workedHours * this.valuePerHour);
    }

    public List<Supply> workForSupply(int humanId, int hours) {
        List<Supply> toReturn = new ArrayList<>();
        if(!produceSupply) return toReturn;
        int workedHours = calculateHours(hours);

        for(int i = 0; i < workedHours; i++) {
            toReturn.add(producedSupply);
        }
        if(workedHours > 0) {
            Logger.logWork(humanId, this, workedHours);
        }
        return toReturn;
    }

    private int calculateHours(int hours) {
        int toReturn = 0;
        if(hours > 0) {
            if (hours < maxHoursPerDay) {
                toReturn = hours;
            } else {
                toReturn = maxHoursPerDay;
            }
        }
        return toReturn;
    }

    public boolean doProduceSupply() {
        return produceSupply;
    }

    public int getMaxHoursPerDay() {
        return maxHoursPerDay;
    }

    @Override
    public String toString() {
        return "Job{" +
                "name='" + name + '\'' +
                ", valuePerHour=" + valuePerHour +
                ", maxHoursPerDay=" + maxHoursPerDay +
                '}';
    }
}
