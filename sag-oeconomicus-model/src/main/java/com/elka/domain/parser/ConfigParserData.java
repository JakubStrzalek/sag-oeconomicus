package com.elka.domain.parser;


import com.elka.domain.AbstractActor;
import com.elka.domain.GlobalConfig;

import java.util.ArrayList;

public class ConfigParserData {
    private ArrayList<AbstractActor> actors;
    private GlobalConfig globalConfig;

    public ConfigParserData(ArrayList<AbstractActor> actors, GlobalConfig globalConfig) {
        this.actors = actors;
        this.globalConfig = globalConfig;
    }

    public ArrayList<AbstractActor> getActors() {
        return actors;
    }

    public void setActors(ArrayList<AbstractActor> actors) {
        this.actors = actors;
    }

    public GlobalConfig getGlobalConfig() {
        return globalConfig;
    }

    public void setGlobalConfig(GlobalConfig globalConfig) {
        this.globalConfig = globalConfig;
    }
}
