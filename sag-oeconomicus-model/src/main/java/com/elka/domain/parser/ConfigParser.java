package com.elka.domain.parser;


import com.elka.domain.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigParser {

    public ConfigParserData parseXML(String xml) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setIgnoringComments(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setCoalescing(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(xml);

        ArrayList<AbstractActor> actors = new ArrayList<>();

        actors.addAll(parseHumans(doc));
        actors.add(parseMarket(doc));
        GlobalConfig globalConfig = parseConfigActor(doc);

        return new ConfigParserData(actors, globalConfig);
    }

    private GlobalConfig parseConfigActor(Document doc) {
        GlobalConfig globalConfig = new GlobalConfig();
        NodeList nList = doc.getElementsByTagName("global");
        if(nList != null){
            Node nNode = nList.item(0);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                globalConfig.setNewDayInterval(Integer.parseInt((eElement.getElementsByTagName("newDayInterval").item(0).getTextContent())));
                globalConfig.setNumberOfDays(Integer.parseInt((eElement.getElementsByTagName("numberOfDays").item(0).getTextContent())));
            }
        }

        return globalConfig;
    }

    private Market parseMarket(Document doc) {
             Market market = new Market();
        NodeList nList = doc.getElementsByTagName("market");
        Node nNode = nList.item(0);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            Element eElement = (Element) nNode;

            NodeList salesObjectsList = eElement.getElementsByTagName("salesObjects");
            ArrayList<SalesObject> salesObjects = new ArrayList<>();
            for (int j = 0; j < salesObjectsList.getLength(); j++) {
                Element salesNode = (Element) salesObjectsList.item(j);
                for (int i = 0; i < salesNode.getChildNodes().getLength(); i++) {
                    if (salesNode.getElementsByTagName("salesObject").item(i) != null) {
                        Element supplyNode = (Element) ((Element) salesNode.getElementsByTagName("salesObject").item(i)).getElementsByTagName("supply").item(0);

                        Element buffNode = (Element) ((Element) salesNode.getElementsByTagName("salesObject").item(i)).getElementsByTagName("buff").item(0);
                        if(supplyNode != null && buffNode != null && supplyNode.getElementsByTagName("buff").item(0) != null &&
                                salesNode.getElementsByTagName("supply").item(0) != null ){
                            Buff buff = new Buff(DemandName.valueOf(buffNode.getElementsByTagName("name").item(0).getTextContent()),
                                    Integer.parseInt(buffNode.getElementsByTagName("value").item(0).getTextContent()));


                            Supply supply = new Supply(supplyNode.getElementsByTagName("name").item(0).getTextContent(),
                                    Double.parseDouble(supplyNode.getElementsByTagName("price").item(0).getTextContent()), buff,
                                    Boolean.parseBoolean(supplyNode.getElementsByTagName("isOneTime").item(0).getTextContent()));

                            SalesObject salesObject = new SalesObject(Integer.parseInt(salesNode.getElementsByTagName("id").item(0).getTextContent()),
                                    Integer.parseInt(salesNode.getElementsByTagName("humanId").item(0).getTextContent()), supply,
                                    Integer.parseInt(salesNode.getElementsByTagName("amount").item(0).getTextContent()));

                            salesObjects.add(salesObject);
                        }

                    }

                }


            }

            market.setId(Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent()));
            market.setSalesObjects(salesObjects);

            System.out.println(market);
        }

        return market;

    }

    private List<Human> parseHumans(Document doc) {
        List<Human> humans = new ArrayList<>();
        NodeList nList = doc.getElementsByTagName("human");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Human human = new Human();
                Element eElement = (Element) nNode;


                human.setId(Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent()));
                human.setName(eElement.getElementsByTagName("name").item(0).getTextContent());

                NodeList jobNode = eElement.getElementsByTagName("job");
                Element jobElem = (Element) jobNode.item(0);
                if(jobElem.getElementsByTagName("producedSupply").getLength() > 0){
                    NodeList producedSupply = eElement.getElementsByTagName("producedSupply");
                    Element supply = (Element) producedSupply.item(0);

                    NodeList buffNode = supply.getElementsByTagName("buff");
                    Element buff = (Element) buffNode.item(0);

                    Buff buf = new Buff(DemandName.valueOf(buff.getElementsByTagName("name").item(0).getTextContent()),
                            Integer.parseInt(buff.getElementsByTagName("value").item(0).getTextContent()));


                    Supply supp = new Supply(supply.getElementsByTagName("name").item(0).getTextContent(),
                            Double.parseDouble(supply.getElementsByTagName("price").item(0).getTextContent()), buf,
                            Boolean.parseBoolean(supply.getElementsByTagName("isOneTime").item(0).getTextContent()));

                    Job job = new Job(jobElem.getElementsByTagName("name").item(0).getTextContent(),
                            Integer.parseInt(jobElem.getElementsByTagName("valuePerHour").item(0).getTextContent()),
                            Integer.parseInt(jobElem.getElementsByTagName("maxHoursPerDay").item(0).getTextContent()), supp);
                    human.setJob(job);
                }
                else{
                    Job job = new Job(jobElem.getElementsByTagName("name").item(0).getTextContent(),
                            Integer.parseInt(jobElem.getElementsByTagName("valuePerHour").item(0).getTextContent()),
                            Integer.parseInt(jobElem.getElementsByTagName("maxHoursPerDay").item(0).getTextContent()));
                    human.setJob(job);
                }




                NodeList demandsList = eElement.getElementsByTagName("demands");
                Map<DemandName, Demand> demands = new HashMap<>();
                for (int j = 0; j < demandsList.getLength(); j++) {
                    Element demandNode = (Element) demandsList.item(j);
                    for (int i = 0; i < demandNode.getChildNodes().getLength(); i++) {
                        if(demandNode.getElementsByTagName("name").item(i) != null){
                            Demand demand = new Demand(DemandName.valueOf(demandNode.getElementsByTagName("name").item(i).getTextContent()),
                                    Integer.parseInt(demandNode.getElementsByTagName("value").item(i).getTextContent()));
                            demands.put(demand.getDemandName(), demand);
                        }
                    }
                }

                human.setDemands(demands);

                NodeList suppliesList = eElement.getElementsByTagName("supplies");

                for (int j = 0; j < suppliesList.getLength(); j++) {
                    Element supplyNode = (Element) suppliesList.item(j);

                    Element buffNode = (Element) supplyNode.getElementsByTagName("buff").item(0);

                    if(buffNode != null){
                        Buff buff = new Buff(DemandName.valueOf(buffNode.getElementsByTagName("name").item(0).getTextContent()),
                                Integer.parseInt(buffNode.getElementsByTagName("value").item(0).getTextContent()));


                        Supply supply = new Supply(supplyNode.getElementsByTagName("name").item(0).getTextContent(),
                                Double.parseDouble(supplyNode.getElementsByTagName("price").item(0).getTextContent()), buff,
                                Boolean.parseBoolean(supplyNode.getElementsByTagName("isOneTime").item(0).getTextContent()));

                        human.addSupply(buff.getDemandName(), supply);
                    }

                }

                human.setMoney(Double.parseDouble(eElement.getElementsByTagName("money").item(0).getTextContent()));

                humans.add(human);
                System.out.println(human);

            }
        }

        return humans;
    }
}
