package com.elka.domain;

public class Demand implements Comparable{

    private DemandName name;
    private int value;

    public Demand(){}

    public Demand(DemandName name, int value) {
        this.name = name;
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }

    public DemandName getDemandName() {
        return this.name;
    }

    public void  fulfill(int value) {
        this.value -= value;
    }

    public void setName(DemandName name) {
        this.name = name;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Demand{" +
                " value=" + value +
                ", name=" + name +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Demand){
            if(this.getValue() < ((Demand) o).getValue()){
                return -1;
            }
            else if(this.getValue() == ((Demand) o).getValue()){
                return 0;
            }
            else {
                return 1;
            }
        }

        return 0;
    }
}
