package com.elka.domain;

public class SalesObject {
    static int lastId = 0;
    int id;
    int humanId;
    private Supply supply;
    private int amountOfSupplies;

    public SalesObject(int id, int humanId, Supply supply, int amount) {
        this.id = id;
        this.humanId = humanId;
        this.supply = supply;
        this.amountOfSupplies = amount;
    }

    public SalesObject(int humanId, Supply supply, int amount) {
        this(lastId + 1, humanId, supply, amount);
        lastId++;
    }

    /**
     * @param amount Ile chcesz kupić towaru z oferty
     * @return Funkcja zwraca to ile kupiłes towaru
     */
    public int substractSupply(int amount) {
        if(amountOfSupplies <= amount) {
            int toReturn = amountOfSupplies;
            amountOfSupplies = 0;
            return toReturn;
        } else {
            this.amountOfSupplies -= amount;
            return amount;
        }
    }

    public int getId() {
        return id;
    }

    public int getHumanId() {
        return humanId;
    }

    public int getAmountOfSupplies() {
        return amountOfSupplies;
    }

    public Supply getSupply() {
        return supply;
    }

    @Override
    public String toString() {
        return "SalesObject{" +
                "id=" + id +
                ", human=" + humanId +
                ", supply=" + supply +
                '}';
    }
}
