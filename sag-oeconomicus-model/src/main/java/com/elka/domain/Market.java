package com.elka.domain;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Market extends AbstractActor {
    int id;
    List<SalesObject> salesObjects;
    List<BuyOffert> buyOfferts;

    public Market() {}

    public Market(int id, List<SalesObject> salesObjects, List<BuyOffert> buyOfferts) {
        this.id = id;
        this.salesObjects = salesObjects;
        this.buyOfferts = buyOfferts;
    }

    public void addNewSalesObject(SalesObject newSalesObject) {
        makeSureSalesObjectsExist();
        salesObjects.add(newSalesObject);
    }

    private void makeSureSalesObjectsExist() {
        if(salesObjects == null){
            salesObjects = new ArrayList<>();
        }
    }

    public void addNewBuyOffert(BuyOffert buyOffert) {
        makeSureBuyOffersExist();
        buyOfferts.add(buyOffert);
    }

    private void makeSureBuyOffersExist() {
        if(buyOfferts == null){
            buyOfferts = new ArrayList<>();
        }
    }

    public List<Pair<BuyOffert, SalesObject>> matchAll() {
        List<Pair<BuyOffert, SalesObject>> toReturn = new ArrayList<>();
        if(buyOfferts != null && salesObjects != null){
            for (BuyOffert buyItem : buyOfferts) {
                for (SalesObject salesItem : salesObjects) {
                    if (salesItem.getSupply().getBuff().getDemandName() == buyItem.getDemandName()) {
                        if (salesItem.getAmountOfSupplies() > 0) {
                            if (buyItem.getPriceLimit() >= (salesItem.getSupply().getPrice() / salesItem.getSupply().getBuff().getValue())) {
                                int getMaxItemsToBuy = (int) (buyItem.getMoney() / salesItem.getSupply().getPrice());
                                int numberOfItemsToBuy = getMaxItemsToBuy > salesItem.getAmountOfSupplies() ? salesItem.getAmountOfSupplies() : getMaxItemsToBuy;

                                SalesObject soldObject = new SalesObject(salesItem.getId(), salesItem.getHumanId(), salesItem.getSupply(), numberOfItemsToBuy);
                                BuyOffert boughtObject = new BuyOffert(buyItem.getHumanId(), salesItem.getSupply().getPrice() * numberOfItemsToBuy,
                                        salesItem.getSupply().getBuff().getDemandName(), buyItem.getPriceLimit());
                                salesItem.substractSupply(numberOfItemsToBuy);
                                buyItem.substract(numberOfItemsToBuy * salesItem.getSupply().getPrice());

                                toReturn.add(new Pair<BuyOffert, SalesObject>(boughtObject, soldObject));
                            }
                        }
                    }
                }
            }
        }


        cleanOffers();
        return toReturn;
    }

    private void cleanOffers() {
        if(buyOfferts != null){
            List<BuyOffert> buyToClean = new ArrayList<>();

            for(BuyOffert buyItem : buyOfferts){
                if(buyItem.getMoney() < 1){
                    buyToClean.add(buyItem);
                }
            }
            buyOfferts.removeAll(buyToClean);
        }

        if(salesObjects != null){
            List<SalesObject> sellToClean = new ArrayList<>();

            for(SalesObject salesItem : salesObjects){
                if(salesItem.getAmountOfSupplies() == 0){
                    sellToClean.add(salesItem);
                }
            }
            salesObjects.removeAll(sellToClean);
        }

    }

    public List<SalesObject> getSalesObjects() {
        return salesObjects;
    }

    public List<BuyOffert> getBuyOfferts() {
        return buyOfferts;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSalesObjects(List<SalesObject> salesObjects) {
        this.salesObjects = salesObjects;
    }

    @Override
    public String toString() {
        return "Market{" +
                "id=" + id +
                ", salesObjects=" + salesObjects +
                '}';
    }

    public int getId() {
        return id;
    }

}
