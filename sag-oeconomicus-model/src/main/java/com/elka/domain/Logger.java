package com.elka.domain;

import java.io.*;
import java.nio.file.*;
import java.util.List;

public abstract class Logger {

    static String PATH_PREFIX = "HUMAN-";
    static String PATH_POSTFIX = ".txt";

    public static void logIdentity(Human human) {
        Path path = Paths.get(PATH_PREFIX + human.getId() + PATH_POSTFIX);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String content = "Spogladajac na moj akt urodzenia widze: \r\n" + human.toString();
        writeToFile(human.getId(), content);
    }

    public static void logNewDay(int humanId, int dayNumber) {
        String content = "\r\n ______________________ NOWY DZIEN " + dayNumber + "  ______________________";
        writeToFile(humanId, content);
    }

    public static void logConsumption(Human human, Supply supply) {
        String content = "Wykorzystuje zasob: \r\n" + supply.toString();
        writeToFile(human.getId(), content);
        logHumanDemands(human);
    }

    public static void logMarketSalesOffers(int humanId, List<SalesObject> salesObjects) {
        if(salesObjects.size() <= 0) return;
        String content = "Wystawilem na bazar nastepujące przedmioty:\r\n";
        for (SalesObject item : salesObjects) {
            content += item.toString() + "\r\n";
        }
        writeToFile(humanId, content);
    }

    public static void logMarketBuysOffers(int humanId, List<BuyOffert> buyObjects) {
        if(buyObjects.size() <= 0) return;
        String content = "Wystawilem na bazar nastepujące oferty kupna:\r\n";
        for (BuyOffert item : buyObjects) {
            content += item.toString() + "\r\n";
        }
        writeToFile(humanId, content);
    }

    public static void logMarketBuysOffers(int humanId, BuyOffert buyObjects) {
        String content = "Wystawilem na bazar nastepująca oferte kupna:\r\n";
        content += buyObjects.toString() + "\r\n";
        writeToFile(humanId, content);
    }

    public static void logWork(int humanId, Job job, int workTime) {
        String content = "Przepracowalem " + workTime + " godzin w: \r\n" + job.toString();
        writeToFile(humanId, content);
    }

    public static void logSleep(int humanId, int hoursSleeped) {
        String content = "Przespalem " + hoursSleeped + "godzin";
        writeToFile(humanId, content);
    }

    public static void logIncomeMoney(int humanId, double money, SalesObject salesdObjects) {
        String content = "SPRZEDALEM " + salesdObjects.getAmountOfSupplies() + salesdObjects.getSupply().getName() + " za " + money + " simuleonow!";
        writeToFile(humanId, content);
    }

    public static void logIncomeSupplies(int humanId, Supply supply, int amountOfSupplies) {
        String content = "KUPILEM " + amountOfSupplies + supply.getName() + " za " + supply.getPrice() * amountOfSupplies + " simuleonow!";
        writeToFile(humanId, content);
    }

    public static void logHumanDemands(Human human) {
        String content = "Moje obecne zapotrzebowanie: \r\n";
        for (DemandName demandName : DemandName.values()) {
            Demand demand = human.getDemands().get(demandName);
            if(demand != null) {
                content += demand.toString() + "\r\n";
            }
        }
        writeToFile(human.getId(), content);
    }

    private static void writeToFile(int humanId, String content) {
        File f = new File(PATH_PREFIX + humanId + PATH_POSTFIX);
        PrintWriter writer = null;
        if(!f.isFile()) {
            try {
                writer = new PrintWriter(PATH_PREFIX + humanId + PATH_POSTFIX, "UTF-8");
                writer.close();
            } catch (FileNotFoundException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        try {
            Files.write(Paths.get(PATH_PREFIX + humanId + PATH_POSTFIX), (content + "\r\n\r\n").getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }
}
