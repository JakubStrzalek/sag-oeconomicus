package com.elka.domain;

public interface Communication {
    void sendToMarket(BuyOffert buyOffert);
    void sendToMarket(SalesObject salesObject);
}
