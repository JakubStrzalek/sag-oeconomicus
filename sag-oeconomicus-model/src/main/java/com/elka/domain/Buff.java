package com.elka.domain;

public class Buff {
    private final DemandName demandName;
    private final int value;

    public Buff(DemandName demandName, int value) {
        this.demandName = demandName;
        this.value = value;
    }

    public DemandName getDemandName() {
        return this.demandName;
    }

    public int getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Buff{" +
                "demandName=" + demandName +
                ", value=" + value +
                '}';
    }
}
