package com.elka.domain;

import com.elka.domain.DemandName;

public class BuyOffert {
    private final int humanId;
    private double money;
    private final DemandName demandName;
    private final double priceLimit;

    @Override
    public String toString() {
        return "BuyOffert{" +
                "humanId=" + humanId +
                ", money=" + money +
                ", demandName=" + demandName +
                ", priceLimit=" + priceLimit +
                '}';
    }

    public BuyOffert(int humanId, double money, DemandName demandName, double priceLimit) {
        this.humanId = humanId;
        this.money = money;
        this.demandName = demandName;
        this.priceLimit = priceLimit;
    }

    public void substract(double money) {
        this.money -= money;
    }

    public int getHumanId() {
        return humanId;
    }

    public double getMoney() {
        return money;
    }

    public DemandName getDemandName() {
        return demandName;
    }

    public double getPriceLimit() {
        return priceLimit;
    }
}
