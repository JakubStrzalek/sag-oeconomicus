package com.elka.domain;

public interface StrategyPlan {
    void makeDecision();
}
