package com.elka.domain;


public class GlobalConfig {
    private int newDayInterval;
    private int numberOfDays;

    public int getNewDayInterval() {
        return newDayInterval;
    }

    public void setNewDayInterval(int newDayInterval) {
        this.newDayInterval = newDayInterval;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }
}
