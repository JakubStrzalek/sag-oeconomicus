package com.elka.domain;

import javafx.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MarketTest {

    @Test
    public void testAddNewSalesObject() throws Exception {
        Market market = new Market(1, new ArrayList<SalesObject>(), new ArrayList<BuyOffert>());

        /**
         * Test start
         */
        market.addNewBuyOffert(new BuyOffert(1, 1, DemandName.ROZRYWKA, 1.0));
        assertEquals(1, market.getBuyOfferts().size());
    }

    @Test
    public void testAddNewBuyOffert() throws Exception {
        Market market = new Market(1, new ArrayList<SalesObject>(), new ArrayList<BuyOffert>());

        /**
         * Test start
         */
        market.addNewSalesObject(new SalesObject(1, 1, new Supply("Banan", 1, new Buff(DemandName.ROZRYWKA, 1), true), 1));
        assertEquals(1, market.getSalesObjects().size());

    }

    @Test
    public void testMatchAllShouldDoCategoryMatch() throws Exception {
        List<SalesObject> tempSalesObjects = new ArrayList<>();
        tempSalesObjects.add(new SalesObject(1, 1, new Supply("banan", 5, new Buff(DemandName.JEDZENIE, 5), true), 1));
        tempSalesObjects.add(new SalesObject(2, 1, new Supply("telewizor", 5, new Buff(DemandName.ROZRYWKA, 5), false), 1));


        List<BuyOffert> tempBuyOffert = new ArrayList<>();
        tempBuyOffert.add(new BuyOffert(1, 5, DemandName.JEDZENIE, 1));
        tempBuyOffert.add(new BuyOffert(1, 5, DemandName.ROZRYWKA, 1));

        Market market = new Market(1, tempSalesObjects, tempBuyOffert);
        List<Pair<BuyOffert, SalesObject>> result = market.matchAll();

        assertEquals(result.get(0).getKey().getDemandName(), DemandName.JEDZENIE);
        assertEquals(result.get(0).getValue().getId(), 1);
        assertEquals(result.get(1).getKey().getDemandName(), DemandName.ROZRYWKA);
        assertEquals(result.get(1).getValue().getId(), 2);
    }

    @Test
    public void testMatchAllShouldDoMatchWithoutCancelingSalesOffer() {
        List<SalesObject> tempSalesObjects = new ArrayList<>();
        tempSalesObjects.add(new SalesObject(1, 1, new Supply("banan", 5, new Buff(DemandName.JEDZENIE, 5), true), 100));

        List<BuyOffert> tempBuyOffert = new ArrayList<>();
        tempBuyOffert.add(new BuyOffert(1, 5, DemandName.JEDZENIE, 1));

        Market market = new Market(1, tempSalesObjects, tempBuyOffert);
        List<Pair<BuyOffert, SalesObject>> result = market.matchAll();

        assertEquals(result.get(0).getKey().getMoney(), 5.0, 0.1);
        assertEquals(result.get(0).getValue().getAmountOfSupplies(), 1);
        assertNotNull(market.getSalesObjects().get(0));
        assertEquals(market.getSalesObjects().get(0).getAmountOfSupplies(), 99);
        assertEquals(market.getBuyOfferts().size(), 0);
    }

    @Test
    public void testMatchAllShouldDoMatchWithoutCancelingBuyOffer() {
        List<SalesObject> tempSalesObjects = new ArrayList<>();
        tempSalesObjects.add(new SalesObject(1, 1, new Supply("banan", 5, new Buff(DemandName.JEDZENIE, 5), true), 2));

        List<BuyOffert> tempBuyOffert = new ArrayList<>();
        tempBuyOffert.add(new BuyOffert(1, 60, DemandName.JEDZENIE, 1));

        Market market = new Market(1, tempSalesObjects, tempBuyOffert);
        List<Pair<BuyOffert, SalesObject>> result = market.matchAll();

        assertEquals(result.get(0).getKey().getMoney(), 10.0, 0.1);
        assertEquals(result.get(0).getValue().getAmountOfSupplies(), 2);
        assertNotNull(market.getBuyOfferts().get(0));
        assertEquals(market.getBuyOfferts().get(0).getMoney(), 50.0, 0.1);
        assertEquals(market.getSalesObjects().size(), 0);
    }

    @Test
    public void testMatchAllShouldMatchMoneyPerUnitGreaterThanPricePerUnit() {
        List<SalesObject> tempSalesObjects = new ArrayList<>();
        tempSalesObjects.add(new SalesObject(1, 1, new Supply("banan", 5, new Buff(DemandName.JEDZENIE, 5), true), 1));

        List<BuyOffert> tempBuyOffert = new ArrayList<>();
        tempBuyOffert.add(new BuyOffert(1, 5, DemandName.JEDZENIE, 10));

        Market market = new Market(1, tempSalesObjects, tempBuyOffert);
        List<Pair<BuyOffert, SalesObject>> result = market.matchAll();

        assertEquals(result.get(0).getKey().getMoney(), 5.0, 0.1);
        assertEquals(result.get(0).getValue().getAmountOfSupplies(), 1);
    }

    @Test
    public void testMatchAllShouldNOTMatchMoneyPerUnitLowerThanPricePerUnit() {
        List<SalesObject> tempSalesObjects = new ArrayList<>();
        tempSalesObjects.add(new SalesObject(1, 1, new Supply("banan", 10, new Buff(DemandName.JEDZENIE, 5), true), 1));

        List<BuyOffert> tempBuyOffert = new ArrayList<>();
        tempBuyOffert.add(new BuyOffert(1, 500, DemandName.JEDZENIE, 1));

        Market market = new Market(1, tempSalesObjects, tempBuyOffert);
        List<Pair<BuyOffert, SalesObject>> result = market.matchAll();

        assertEquals(result.size(), 0);
    }
}