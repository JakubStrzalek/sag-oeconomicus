package com.elka.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void testConsumeSupply() throws Exception {
        Map<DemandName, Demand> demands = new HashMap<>();
        demands.put(DemandName.JEDZENIE, new Demand(DemandName.JEDZENIE, 10));
        demands.put(DemandName.ROZRYWKA, new Demand(DemandName.ROZRYWKA, 10));

        Map<DemandName, List<Supply>> supplies = new HashMap<>();

        List<Supply> foodSupplies = new ArrayList<Supply>();
        foodSupplies.add(new Supply("Banan", 1.0, new Buff(DemandName.JEDZENIE, 1), true));
        supplies.put(DemandName.JEDZENIE, foodSupplies);

        List<Supply> joySupplies = new ArrayList<Supply>();
        joySupplies.add(new Supply("Telewizor", 1.0, new Buff(DemandName.ROZRYWKA, 1), false));
        supplies.put(DemandName.ROZRYWKA, joySupplies);

        Human human = new Human(1, demands, null, supplies, 0);

        /**
         * Test start
         */
        human.consumeSupply(DemandName.JEDZENIE, "Banan");
        assertEquals(9, human.getDemands().get(DemandName.JEDZENIE).getValue());
        assertEquals(23, human.getActionPoints(), 0.1);

        human.consumeSupply(DemandName.ROZRYWKA, "Telewizor");
        assertEquals(9, human.getDemands().get(DemandName.ROZRYWKA).getValue());
        assertNotNull(human.getSupplies().get(DemandName.ROZRYWKA).get(0));
        assertEquals(22, human.getActionPoints(), 0.1);
    }

    @Test
    public void testWork() throws Exception {
        Map<DemandName, Demand> demands = new HashMap<>();
        demands.put(DemandName.KARIERA, new Demand(DemandName.KARIERA, 10));

        Human human = new Human(1, demands, new Job("PiesBezNogi", 1, 8), null, 0);

        /**
         * Test start
         */
        human.work(-8);
        assertEquals(0, human.getMoney(), 0.1);
        assertEquals(24, human.getActionPoints(), 0.1);

        human.work(1);
        assertEquals(1, human.getMoney(), 0.1);
        assertEquals(23, human.getActionPoints(), 0.1);

        human.work(8);
        assertEquals(9, human.getMoney(), 0.1);
        assertEquals(15, human.getActionPoints(), 0.1);

        human.work(20);
        assertEquals(17, human.getMoney(), 0.1);
        assertEquals(7, human.getActionPoints(), 0.1);

    }

    @Test
    public void testSleep() throws Exception {
        Map<DemandName, Demand> demands = new HashMap<>();
        demands.put(DemandName.SEN, new Demand(DemandName.SEN, 30));

        Human human = new Human(1, demands, null, null, 0);

        /**
         * Test start
         */
        human.sleep(-3);
        assertEquals(30, human.getDemands().get(DemandName.SEN).getValue(), 0.1);
        assertEquals(24, human.getActionPoints(), 0.1);

        human.sleep(2);
        assertEquals(28, human.getDemands().get(DemandName.SEN).getValue(), 0.1);
        assertEquals(22, human.getActionPoints(), 0.1);

        human.sleep(100);
        assertEquals(6, human.getDemands().get(DemandName.SEN).getValue(), 0.1);
        assertEquals(0, human.getActionPoints(), 0.1);
    }

    @Test
    public void testGetTwoMostImportandDemands() throws Exception {
        Map<DemandName, Demand> demands = new HashMap<>();
        demands.put(DemandName.SEN, new Demand(DemandName.SEN, 50));
        demands.put(DemandName.JEDZENIE, new Demand(DemandName.JEDZENIE, 50));
        demands.put(DemandName.ROZRYWKA, new Demand(DemandName.ROZRYWKA, 50));
        demands.put(DemandName.KARIERA, new Demand(DemandName.KARIERA, 50));

        Human human = new Human(1, demands, null, null, 0);

        /**
         * Test start
         */
        Map<String, Demand> result = human.getTwoMostImportandDemands();
        assertEquals(result.get("First").getValue(), 50);
        assertEquals(result.get("Second").getValue(), 50);
        assertNotEquals(result.get("First").getDemandName(), result.get("Second").getDemandName());
    }
}