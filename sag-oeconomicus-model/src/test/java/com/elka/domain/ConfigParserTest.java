package com.elka.domain;


import com.elka.domain.parser.ConfigParser;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class ConfigParserTest {

    @Test
    public void testConfigParserOutputsHumans() throws ParserConfigurationException, SAXException, IOException {
        ConfigParser configParser = new ConfigParser();
        configParser.parseXML("appConfig.xml");
    }
}
